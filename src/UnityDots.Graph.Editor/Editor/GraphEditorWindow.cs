using System;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    public sealed class GraphEditorWindow : EditorWindow
    {
        private GraphData _prevGraph;
        private const string _invalidGraphTitle = "<No Graph>";

        private GraphEditorView _editorView;
        private GraphEditorView EditorView
        {
            get => _editorView;
            set => SetGraphEditorView(value);
        }

        [MenuItem("Window/Skadi Terrain Generator")]
        public static void Init()
        {
            var window = GetWindow<GraphEditorWindow>();
            window.titleContent = new GUIContent(_invalidGraphTitle);
            window.Show();
            window.OpenGraph();
        }

        [OnOpenAsset(0)]
        public static bool OnOpenGraph(int instanceID, int _)
        {
            var graph = EditorUtility.InstanceIDToObject(instanceID) as GraphData;
            if (graph == null)
            {
                return false;
            }

            var window = GetWindow<GraphEditorWindow>();
            window.OpenGraph(graph);
            return true;
        }

        private void OnEnable()
        {
            OpenGraph();
        }

        private void OnDisable()
        {
            CloseGraph();
        }

        private void OnSelectionChange()
        {
            OpenGraph();
        }

        private void OpenGraph()
        {
            var graph = Selection.activeObject as GraphData ?? _prevGraph;
            if (graph != null)
            {
                OpenGraph(graph);
            }
        }

        private void OpenGraph(GraphData graph)
        {
            if (EditorView != null && graph == EditorView.Graph)
            {
                return;
            }

            if (graph == null)
            {
                return;
            }

            try
            {
                CloseGraph();

                // Reload graph to make sure all scriptableobject references are a-okay
                // https://issuetracker.unity3d.com/issues/scriptableobject-references-become-temporarily-null-in-editor-when-they-are-modified-outside-of-unity-and-then-reloaded
                graph = GraphDataSaver.Reload(graph);
                GraphDataValidator.ValidateGraph(graph);

                titleContent = new GUIContent(graph.name);
                EditorView = CustomViewCreator.CreateEditorView(graph.GetType());
                EditorView.Open(this, graph);
                EditorView.RegisterCallback<GeometryChangedEvent>(OnPostLayout);
                _prevGraph = graph;

                Repaint();
            }
            catch (Exception)
            {
                EditorView = null;
                throw;
            }
        }

        private void CloseGraph()
        {
            if (EditorView != null)
            {
                GraphDataSaver.Save(EditorView.Graph, true);
                GraphDataSaver.Unload(EditorView.Graph);

                EditorView.Close();
                EditorView.UnregisterCallback<GeometryChangedEvent>(OnPostLayout);
                EditorView = null;
            }
        }

        private void SetGraphEditorView(GraphEditorView value)
        {
            if (_editorView != null)
            {
                _editorView.RemoveFromHierarchy();
            }

            _editorView = value;

            if (_editorView != null)
            {
                rootVisualElement.Add(_editorView);
            }
        }

        private void OnPostLayout(GeometryChangedEvent evt)
        {
            EditorView.UnregisterCallback<GeometryChangedEvent>(OnPostLayout);
            EditorView.GraphView.FrameAll();
        }
    }
}
