using System;

namespace UnityDots.Graph.Editors
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CustomGraphEditorViewAttribute : Attribute
    {
        public Type Type { get; }

        public CustomGraphEditorViewAttribute(Type type)
        {
            Type = type;
        }
    }
}
