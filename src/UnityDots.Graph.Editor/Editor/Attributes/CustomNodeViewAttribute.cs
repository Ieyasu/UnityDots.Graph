using System;

namespace UnityDots.Graph.Editors
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CustomNodeViewAttribute : Attribute
    {
        public Type Type { get; }
        public bool EditorForChildClasses { get; }

        public CustomNodeViewAttribute(Type type, bool editorForChildClasses = false)
        {
            Type = type;
            EditorForChildClasses = editorForChildClasses;
        }
    }
}
