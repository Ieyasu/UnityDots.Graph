using UnityEngine;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Helper class for building the data representation of the graph.
    /// </summary>
    public static class GraphDataValidator
    {
        public static void ValidateGraph(GraphData graph)
        {
            ValidateNodes(graph);
            ValidateEdges(graph);

            // AssetDatabase.SaveAssets();
        }

        public static void ValidateNodes(GraphData graph)
        {
            if (graph == null)
            {
                return;
            }

            // Remove invalid nodes.
            for (var i = graph.Nodes.Count - 1; i >= 0; --i)
            {
                if (IsInvalidNode(graph, i) || IsDuplicateNode(graph, i))
                {
                    Debug.LogError($"Removing invalid node {graph.Nodes[i]}");
                    graph.RemoveNodeAt(i);
                }
            }
        }

        public static void ValidateEdges(GraphData graph)
        {
            // Remove invalid edges.
            for (var i = graph.Edges.Count - 1; i >= 0; --i)
            {
                if (IsInvalidEdge(graph, i) || IsDuplicateEdge(graph, i))
                {
                    Debug.LogError($"Removing invalid edge {graph.Edges[i]}");
                    graph.RemoveEdgeAt(i);
                }
            }
        }

        private static bool IsInvalidNode(GraphData graph, int index)
        {
            return graph.Nodes[index] == null;
        }

        private static bool IsDuplicateNode(GraphData graph, int index)
        {
            var node = graph.Nodes[index];
            for (var i = 0; i < graph.Nodes.Count - 1; ++i)
            {
                var other = graph.Nodes[i];
                if (i != index && other != null && (node == other || node.name == other.name))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsInvalidEdge(GraphData graph, int index)
        {
            var edge = graph.Edges[index];
            if (edge.Start.Node == null || edge.End.Node == null)
            {
                return true;
            }

            if (!edge.Start.Node.HasOutputSocket(edge.Start.SocketID))
            {
                return true;
            }

            if (!edge.End.Node.HasInputSocket(edge.End.SocketID))
            {
                return true;
            }

            return false;
        }

        private static bool IsDuplicateEdge(GraphData graph, int index)
        {
            var edge = graph.Edges[index];
            for (var i = 0; i < graph.Edges.Count - 1; ++i)
            {
                var other = graph.Edges[i];
                if (i != index && edge.Equals(other))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
