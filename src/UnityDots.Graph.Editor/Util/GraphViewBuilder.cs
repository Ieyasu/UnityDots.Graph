using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    public static class GraphViewBuilder
    {
        public static void SyncViewToData(ExtendedGraphView graphView, GraphData graph)
        {
            SyncNodesToGraph(graphView, graph);
            SyncEdgesToGraph(graphView, graph);
        }

        private static void SyncNodesToGraph(ExtendedGraphView graphView, GraphData graph)
        {
            // Remove existing node views that don't have a match in the graph data.
            var nodeViews = graphView.nodes.ToList();
            foreach (var nodeView in nodeViews.OfType<NodeView>())
            {
                if (!graph.ContainsNode(nodeView.Node))
                {
                    graphView.RemoveElement(nodeView);
                }
            }

            // Add missing node views based on the graph data and synchronize data positions.
            foreach (var node in graph.Nodes)
            {
                var nodeView = GetNodeView(nodeViews, node);
                if (nodeView == null)
                {
                    nodeView = CustomViewCreator.CreateNodeView(node.GetType());
                    AddNodeView(graphView, node, nodeView);
                }
                else if (IsNodeDataOutOfSync(node, nodeView))
                {
                    nodeView.SyncViewToData();
                }
            }
        }

        private static void SyncEdgesToGraph(ExtendedGraphView graphView, GraphData graph)
        {
            // Remove existing edge views that don't have a match in the graph data.
            var edgeViews = graphView.edges.ToList();
            var nodeViews = graphView.nodes.ToList();
            foreach (var edgeView in edgeViews)
            {
                if (!(edgeView.userData is EdgeData edge) || !graph.Edges.Contains(edge))
                {
                    graphView.RemoveElement(edgeView);
                    RefreshExpandedState(edgeView);
                }
            }

            // Add missing edge views based on the graph data.
            foreach (var edge in graph.Edges)
            {
                var edgeView = GetEdgeView(edgeViews, edge);
                if (edgeView == null)
                {
                    edgeView = CreateEdgeView(nodeViews, edge);
                    AddEdgeView(graphView, edgeView);
                    RefreshExpandedState(edgeView);
                }
            }
        }

        private static NodeView GetNodeView(List<Node> nodeViews, NodeData node)
        {
            foreach (var nodeView in nodeViews)
            {
                if (node.Equals(nodeView.userData))
                {
                    return nodeView as NodeView;
                }
            }
            return null;
        }

        private static Edge GetEdgeView(List<Edge> edgeViews, EdgeData edge)
        {
            foreach (var edgeView in edgeViews)
            {
                if (edgeView.userData is EdgeData other && edge.Equals(other))
                {
                    return edgeView;
                }
            }
            return null;
        }

        private static void AddNodeView(ExtendedGraphView graphView, NodeData node, NodeView nodeView)
        {
            if (nodeView == null)
            {
                Debug.LogError("NodeView is null");
                return;
            }

            graphView.AddNode(node, nodeView);
        }

        private static void AddEdgeView(ExtendedGraphView graphView, Edge edgeView)
        {
            if (edgeView == null)
            {
                Debug.LogError("Failed to add EdgeView: is null");
                return;
            }

            edgeView.input.Connect(edgeView);
            edgeView.output.Connect(edgeView);
            graphView.AddElement(edgeView);
        }

        private static Edge CreateEdgeView(List<Node> nodeViews, EdgeData edge)
        {
            if (edge.End.Node == null || edge.Start.Node == null)
            {
                Debug.LogError($"Failed to build EdgeView: null node connecting to {edge}.");
                return null;
            }

            var startNodeView = GetNodeView(nodeViews, edge.Start.Node);
            var endNodeView = GetNodeView(nodeViews, edge.End.Node);
            var outputSocketView = startNodeView.GetOutputSocket(edge.Start.SocketID);
            var inputSocketView = endNodeView.GetInputSocket(edge.End.SocketID);

            var edgeView = new Edge
            {
                userData = edge,
                output = outputSocketView,
                input = inputSocketView
            };

            return edgeView;
        }

        private static void RefreshExpandedState(Edge edgeView)
        {
            var inputPortView = edgeView.input as PortView;
            var outputPortView = edgeView.output as PortView;
            if (inputPortView != null)
            {
                inputPortView.NodeView.RefreshExpandedState();
            }
            if (outputPortView != null)
            {
                outputPortView.NodeView.RefreshExpandedState();
            }
        }

        private static bool IsNodeDataOutOfSync(NodeData node, NodeView nodeView)
        {
            return node.ViewData.Position != nodeView.GetPosition().position
                    || node.ViewData.IsExpanded != nodeView.expanded
                    || node.ViewData.IsPreviewExpanded != nodeView.Preview.IsExpanded;
        }
    }
}
