using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    public struct GraphCopyPasteData
    {
        public Vector2 CopyPosition { get; }
        public List<NodeData> Nodes { get; }
        public List<EdgeData> Edges { get; }

        public GraphCopyPasteData(Vector2 copyPosition)
        {
            CopyPosition = copyPosition;
            Nodes = new List<NodeData>();
            Edges = new List<EdgeData>();
        }
    }

    public static class GraphDataSerializer
    {
        private const string _serializeMagic = "SERIALIZED_GRAPH";

        public static string SerializeGraphElements(Vector2 copyPosition, IEnumerable<GraphElement> elements)
        {
            var nodes = elements
                    .Where(x => x is NodeView && x.userData is NodeData)
                    .Select(x => x.userData as NodeData)
                    .ToList();

            var edges = elements
                    .Where(x => x is Edge && x.userData is EdgeData)
                    .Select(x => (EdgeData)x.userData)
                    .Where(x => nodes.IndexOf(x.Start.Node) != -1 && nodes.IndexOf(x.End.Node) != -1)
                    .ToList();

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(_serializeMagic);
                writer.Write(copyPosition.x);
                writer.Write(copyPosition.y);
                writer.Write(nodes.Count);
                writer.Write(edges.Count);

                foreach (var node in nodes)
                {
                    var json = JsonUtility.ToJson(node);
                    writer.Write(node.GetType().AssemblyQualifiedName);
                    writer.Write(json);
                }

                foreach (var edge in edges)
                {
                    var startIndex = nodes.IndexOf(edge.Start.Node);
                    var endIndex = nodes.IndexOf(edge.End.Node);
                    writer.Write(startIndex);
                    writer.Write(edge.Start.SocketID);
                    writer.Write(endIndex);
                    writer.Write(edge.End.SocketID);
                }

                writer.Flush();

                return Convert.ToBase64String(stream.GetBuffer());
            }
        }

        public static bool CanPasteSerializedData(string data)
        {
            try
            {
                var bytes = Convert.FromBase64String(data);
                using (var stream = new MemoryStream(bytes))
                using (var reader = new BinaryReader(stream))
                {
                    return reader.ReadString() == _serializeMagic;
                }
            }
            catch
            {
                return false;
            }
        }

        public static GraphCopyPasteData DeserializeAndPaste(string data)
        {
            var bytes = Convert.FromBase64String(data);

            using (var stream = new MemoryStream(bytes))
            using (var reader = new BinaryReader(stream))
            {
                var magic = reader.ReadString();
                var copyPosX = reader.ReadSingle();
                var copyPosY = reader.ReadSingle();
                var nodeCount = reader.ReadInt32();
                var edgeCount = reader.ReadInt32();

                var deserializedData = new GraphCopyPasteData(new Vector2(copyPosX, copyPosY));

                for (var i = 0; i < nodeCount; ++i)
                {
                    var typeName = reader.ReadString();
                    var type = Type.GetType(typeName);
                    var json = reader.ReadString();
                    var node = ScriptableObject.CreateInstance(type) as NodeData;
                    JsonUtility.FromJsonOverwrite(json, node);
                    deserializedData.Nodes.Add(node);
                }

                for (var i = 0; i < edgeCount; ++i)
                {
                    var startIndex = reader.ReadInt32();
                    var startSocketID = reader.ReadInt32();
                    var startNode = deserializedData.Nodes[startIndex];
                    var endIndex = reader.ReadInt32();
                    var endSocketID = reader.ReadInt32();
                    var endNode = deserializedData.Nodes[endIndex];
                    var edge = new EdgeData(startNode, startSocketID, endNode, endSocketID);
                    deserializedData.Edges.Add(edge);
                }

                return deserializedData;
            }
        }
    }
}
