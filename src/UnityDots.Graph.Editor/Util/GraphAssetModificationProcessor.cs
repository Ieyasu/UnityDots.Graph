using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    // Hack required to fix issue with subassets when renaming a main asset
    // https://issuetracker.unity3d.com/issues/parent-and-child-nested-scriptable-object-assets-switch-places-when-parent-scriptable-object-asset-is-renamed
    public sealed class GraphAssetModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        private static AssetMoveResult OnWillMoveAsset(string sourcePath, string destinationPath)
        {
            var graph = AssetDatabase.LoadMainAssetAtPath(sourcePath) as GraphData;
            if (graph == null)
            {
                return AssetMoveResult.DidNotMove;
            }

            var sourceDir = Path.GetDirectoryName(sourcePath);
            var destinationDir = Path.GetDirectoryName(destinationPath);
            if (sourceDir != destinationDir)
            {
                return AssetMoveResult.DidNotMove;
            }

            var fileName = Path.GetFileNameWithoutExtension(destinationPath);
            graph.name = fileName;
            return AssetMoveResult.DidNotMove;
        }
    }
}
