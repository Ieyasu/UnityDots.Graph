using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Helper class for building the data representation of the graph.
    /// </summary>
    public static class GraphDataBuilder
    {
        public static void AddNode(GraphData graph, Vector2 position, NodeData node)
        {
            EditorUtility.SetDirty(graph);
            Undo.RegisterCreatedObjectUndo(node, "Create Node");
            Undo.RegisterFullObjectHierarchyUndo(graph, "Add Node to Graph");

            node.name = GetName(graph, node.GetType());
            node.ViewData = new NodeViewData
            {
                Position = position,
                IsExpanded = true,
                IsPreviewExpanded = true
            };

            graph.AddNode(node);
            AssetDatabase.AddObjectToAsset(node, graph);
        }

        public static NodeData AddNode(GraphData graph, Vector2 position, Type nodeType)
        {
            var node = ScriptableObject.CreateInstance(nodeType) as NodeData;
            AddNode(graph, position, node);
            return node;
        }

        public static bool AddEdge(GraphData graph, EdgeData edge)
        {
            Undo.RegisterFullObjectHierarchyUndo(graph, "Add Edge to Graph");

            if (!graph.AddEdge(edge, true))
            {
                return false;
            }

            EditorUtility.SetDirty(graph);
            return true;
        }

        public static EdgeData? AddEdge(GraphData graph, Edge edgeView)
        {
            if (!(edgeView.input is PortView inputPort) || !(edgeView.output is PortView outputPort))
            {
                return null;
            }

            var inputNode = inputPort.NodeView.Node;
            var outputNode = outputPort.NodeView.Node;
            var edge = new EdgeData(outputNode, outputPort.ID, inputNode, inputPort.ID);

            if (!AddEdge(graph, edge))
            {
                return null;
            }

            edgeView.userData = edge;
            return edge;
        }

        public static bool RemoveNode(GraphData graph, NodeView nodeView)
        {
            Undo.RegisterFullObjectHierarchyUndo(graph, "Remove Node from Graph");

            if (nodeView.userData == null)
            {
                return false;
            }

            var node = nodeView.userData as NodeData;
            if (!graph.RemoveNode(node))
            {
                return false;
            }

            EditorUtility.SetDirty(graph);
            Undo.DestroyObjectImmediate(node);
            return true;
        }

        public static bool RemoveEdge(GraphData graph, Edge edgeView)
        {
            Undo.RegisterFullObjectHierarchyUndo(graph, "Remove Edge from Graph");

            if (edgeView.userData == null)
            {
                return false;
            }

            var edge = (EdgeData)edgeView.userData;
            if (!graph.RemoveEdge(edge))
            {
                return false;
            }

            EditorUtility.SetDirty(graph);
            return true;
        }

        public static void RemoveNodes(GraphData graph, IEnumerable<NodeView> nodeViews)
        {
            Undo.RegisterFullObjectHierarchyUndo(graph, "Remove Nodes from Graph");

            foreach (var nodeView in nodeViews)
            {
                if (nodeView.userData is NodeData node && graph.RemoveNode(node))
                {
                    Undo.DestroyObjectImmediate(node);
                    EditorUtility.SetDirty(graph);
                }
            }
        }

        public static void RemoveEdges(GraphData graph, IEnumerable<Edge> edgeViews)
        {
            Undo.RegisterFullObjectHierarchyUndo(graph, "Remove Edges from Graph");

            foreach (var edgeView in edgeViews)
            {
                if (edgeView.userData is EdgeData edge && graph.RemoveEdge(edge))
                {
                    EditorUtility.SetDirty(graph);
                }
            }
        }

        public static void SyncNodeDataToView(GraphData graph, IEnumerable<NodeView> nodeViews)
        {
            foreach (var nodeView in nodeViews)
            {
                SyncNodeDataToView(graph, nodeView);
            }
        }

        public static void SyncNodeDataToView(GraphData graph, NodeView nodeView)
        {
            if (nodeView.userData is NodeData node && IsNodeDataOutOfSync(node, nodeView))
            {
                Undo.RecordObject(node, "Change Node");
                node.ViewData = new NodeViewData
                {
                    Position = nodeView.GetPosition().position,
                    IsExpanded = nodeView.expanded,
                    IsPreviewExpanded = nodeView.Preview.IsExpanded
                };
                EditorUtility.SetDirty(graph);
            }
        }

        private static string GetName(GraphData graph, Type nodeType)
        {
            var id = 0;
            var name = $"{nodeType.Name}_{id}";
            while (graph.Nodes.Any(x => x.name == name))
            {
                name = $"{nodeType.Name}_{++id}";
            }
            return name;
        }

        private static bool IsNodeDataOutOfSync(NodeData node, NodeView nodeView)
        {
            return node.ViewData.Position != nodeView.GetPosition().position
                    || node.ViewData.IsExpanded != nodeView.expanded
                    || node.ViewData.IsPreviewExpanded != nodeView.Preview.IsExpanded;
        }
    }
}
