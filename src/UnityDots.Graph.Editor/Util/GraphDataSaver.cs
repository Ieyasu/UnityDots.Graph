using System.Linq;
using UnityDots.Editor;
using UnityEditor;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Helper class saving graph data.
    /// </summary>
    public static class GraphDataSaver
    {
        public static void Save(GraphData graph, bool ask = false)
        {
            if (!IsDirty(graph))
            {
                return;
            }

            var msg = "Do you want to save the changes you made in the graph?\n"
                       + AssetDatabase.GetAssetPath(graph)
                       + "\n\nYour changes will be lost if you don't save them.";

            if (!ask || EditorUtility.DisplayDialog("Graph Has Been Modified", msg, "Save", "Don't Save"))
            {
                AssetDatabase.SaveAssets();
            }
        }

        public static GraphData Reload(GraphData oldGraph)
        {
            // Reload to make sure all references to other ScriptableObjects are up to date
            var path = AssetDatabase.GetAssetPath(oldGraph);
            Unload(oldGraph);

            var graph = AssetDatabase.LoadMainAssetAtPath(path) as GraphData;
            if (graph == null)
            {
                throw new System.InvalidOperationException($"Failed to load graph from path {path}");
            }

            var assets = AssetDatabase.LoadAllAssetsAtPath(path);
            foreach (var asset in assets)
            {
                if (asset is NodeData node && !graph.Nodes.Contains(node))
                {
                    graph.AddNode(node);
                }
            }
            return graph;
        }

        public static void Unload(GraphData graph)
        {
            foreach (var node in graph.Nodes)
            {
                Resources.UnloadAsset(node);
            }

            AssetDatabaseHelper.DiscardChanges(graph);
            Resources.UnloadAsset(graph);
        }

        public static bool IsDirty(GraphData graph)
        {
            if (EditorUtility.IsDirty(graph))
            {
                return true;
            }

            return graph.Nodes.Any(x => EditorUtility.IsDirty(x));
        }
    }
}
