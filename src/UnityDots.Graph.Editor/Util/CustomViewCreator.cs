using System;
using System.Collections.Generic;
using System.Reflection;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Helper class for creating custom views.
    /// </summary>
    public static class CustomViewCreator
    {
        private static readonly Dictionary<Type, Type> _nodeViewMap = new Dictionary<Type, Type>();
        private static readonly Dictionary<Type, Type> _editorViewMap = new Dictionary<Type, Type>();

        static CustomViewCreator()
        {
            BuildMaps();
        }

        public static GraphEditorView CreateEditorView(Type graphType)
        {
            if (_editorViewMap.TryGetValue(graphType, out Type viewType))
            {
                return Activator.CreateInstance(viewType) as GraphEditorView;
            }

            return new GraphEditorView();
        }

        public static NodeView CreateNodeView(Type nodeType)
        {
            if (_nodeViewMap.TryGetValue(nodeType, out Type viewType))
            {
                return Activator.CreateInstance(viewType) as NodeView;
            }

            return new NodeView();
        }

        private static void BuildMaps()
        {
            foreach (var type in TypeExtensions.GetAllTypes())
            {
                if (type.IsSubclassOf(typeof(NodeView)))
                {
                    var nodeViewAttribute = type.GetCustomAttribute<CustomNodeViewAttribute>(false);
                    if (nodeViewAttribute != null)
                    {
                        AddNodeViewEditor(type, nodeViewAttribute);
                    }
                }
                else if (type.IsSubclassOf(typeof(GraphEditorView)))
                {
                    var editorViewAttribute = type.GetCustomAttribute<CustomGraphEditorViewAttribute>(false);
                    if (editorViewAttribute != null && !_editorViewMap.ContainsKey(editorViewAttribute.Type))
                    {
                        _editorViewMap.Add(editorViewAttribute.Type, type);
                    }
                }
            }
        }

        private static void AddNodeViewEditor(Type editorType, CustomNodeViewAttribute attribute)
        {
            if (!attribute.EditorForChildClasses)
            {
                _nodeViewMap[attribute.Type] = editorType;
            }
            else
            {
                foreach (var type in TypeExtensions.GetAllTypes())
                {
                    if (type.IsSubclassOf(attribute.Type) && !_nodeViewMap.ContainsKey(type))
                    {
                        _nodeViewMap[type] = editorType;
                    }
                }
            }
        }
    }
}
