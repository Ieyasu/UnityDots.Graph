using System;
using System.Collections.Generic;
using System.Linq;
using UnityDots.Editor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// A sensible standard implementation for GraphView.
    /// </summary>
    public sealed class ExtendedGraphView : GraphView
    {
        public Action<NodeView> NodeChanged;

        private readonly GraphData _graph;
        private readonly IEdgeConnectorListener _edgeConnector;

        public ExtendedGraphView(GraphData graph, IEdgeConnectorListener edgeConnector)
        {
            this.LoadAndAddStyleSheet("Styles/GraphView");
            AddToClassList("unitydots-graph-view");
            name = "StandardGraphView";

            _graph = graph;
            _edgeConnector = edgeConnector;

            SetupZoom(0.05f, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new ClickSelector());
        }

        public void AddNode(NodeData node, NodeView nodeView)
        {
            nodeView.Initialize(node, _edgeConnector);
            nodeView.NodeChanged = () => { NodeChanged?.Invoke(nodeView); };
            AddElement(nodeView);
        }

        public override List<Port> GetCompatiblePorts(Port startAnchor, NodeAdapter nodeAdapter)
        {
            var compatibleAnchors = new List<Port>();

            ports.ForEach(endAnchor =>
            {
                if (PortsAreCompatible(startAnchor, endAnchor))
                {
                    compatibleAnchors.Add(endAnchor);
                }
            });

            return compatibleAnchors;
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent e)
        {
            base.BuildContextualMenu(e);

            if (e.target is GraphView)
            {
                InitializeNodeContextMenu(e);
                e.menu.AppendSeparator();
                InitializeGlobalContextMenu(e);
            }
            else if (e.target is Node)
            {
                InitializeNodeContextMenu(e);
            }

            e.menu.AppendSeparator();
        }

        private bool PortsAreCompatible(Port start, Port end)
        {
            return start.node != end.node
                && end.direction != start.direction
                && _graph.IsValidEdge(start.portType, end.portType);
        }

        private void InitializeGlobalContextMenu(ContextualMenuPopulateEvent e)
        {
            foreach (var nodeView in nodes.ToList().OfType<NodeView>())
            {
                if (nodeView.HasPreview && nodeView.Preview.IsExpanded)
                {
                    e.menu.AppendAction("Collapse All Previews", _ => ExpandAllPreviews(false), a => DropdownMenuAction.Status.Normal);
                }
                if (nodeView.HasPreview && !nodeView.Preview.IsExpanded)
                {
                    e.menu.AppendAction("Expand All Previews", _ => ExpandAllPreviews(true), a => DropdownMenuAction.Status.Normal);
                }
            }
        }

        private void InitializeNodeContextMenu(ContextualMenuPopulateEvent e)
        {
            var expandPreviewText = (selection.Count == 1) ? "View/Expand Preview" : "View/Expand Previews";
            var collapsePreviewText = (selection.Count == 1) ? "View/Collapse Preview" : "View/Collapse Previews";
            var expandControlsText = "View/Expand Controls";
            var collapseControlsText = "View/Collapse Controls";

            // Check if we can expand or collapse the ports/previews
            var expandPreviewAction = DropdownMenuAction.Status.Disabled;
            var collapsePreviewAction = DropdownMenuAction.Status.Disabled;
            var minimizeAction = DropdownMenuAction.Status.Disabled;
            var maximizeAction = DropdownMenuAction.Status.Disabled;
            foreach (var nodeView in selection.OfType<NodeView>())
            {
                if (nodeView.HasPreview && nodeView.Preview.IsExpanded)
                {
                    collapsePreviewAction = DropdownMenuAction.Status.Normal;
                }
                else if (nodeView.HasPreview && !nodeView.Preview.IsExpanded)
                {
                    expandPreviewAction = DropdownMenuAction.Status.Normal;
                }
                if (nodeView.expanded)
                {
                    minimizeAction = DropdownMenuAction.Status.Normal;
                }
                else if (!nodeView.expanded)
                {
                    maximizeAction = DropdownMenuAction.Status.Normal;
                }
            }

            e.menu.AppendAction(collapseControlsText, _ => ExpandSelectedNodeControls(false), (a) => minimizeAction);
            e.menu.AppendAction(expandControlsText, _ => ExpandSelectedNodeControls(true), (a) => maximizeAction);
            e.menu.AppendSeparator("View/");
            e.menu.AppendAction(expandPreviewText, _ => ExpandSelectedNodePreviews(true), (a) => expandPreviewAction);
            e.menu.AppendAction(collapsePreviewText, _ => ExpandSelectedNodePreviews(false), (a) => collapsePreviewAction);
        }

        private void ExpandAllPreviews(bool expand)
        {
            foreach (var nodeView in nodes.ToList().OfType<NodeView>())
            {
                nodeView.Preview.IsExpanded = expand;
            }
        }

        public void ExpandSelectedNodeControls(bool isExpanded)
        {
            foreach (var nodeView in selection.OfType<NodeView>())
            {
                nodeView.expanded = isExpanded;
            }
        }

        public void ExpandSelectedNodePreviews(bool isExpanded)
        {
            foreach (var nodeView in selection.OfType<NodeView>())
            {
                nodeView.Preview.IsExpanded = isExpanded;
            }
        }
    }
}
