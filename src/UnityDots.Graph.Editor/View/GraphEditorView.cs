using System;
using System.Collections.Generic;
using System.Linq;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Root visual element which takes up entire editor window. Every other visual element is a child
    /// of this.
    /// </summary>
    public class GraphEditorView : VisualElement
    {
        private Vector2 _contextMousePos;
        private GraphEditorWindow _editorWindow;
        private SearchWindowProvider _searchWindowProvider;

        public GraphData Graph { get; private set; }
        public ExtendedGraphView GraphView { get; private set; }

        protected virtual void OnOpen() { }
        protected virtual void OnClose() { }
        protected virtual void OnGraphChange() { }

        public void Open(GraphEditorWindow editorWindow, GraphData graph)
        {
            _editorWindow = editorWindow;
            Graph = graph;

            OnOpen();
            CreateView();
            RegisterCallbacks();
        }

        public void Close()
        {
            UnregisterCallbacks();
            OnClose();
        }

        protected virtual void CreateView()
        {
            this.LoadAndAddStyleSheet("Styles/CustomGraphEditorView");
            AddToClassList("unitydots-graph-editor-view");

            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(Graph, out string guid, out long _);
            viewDataKey = guid;

            CreateSearchWindowProvider();
            CreateGraphView();
        }

        private void RegisterCallbacks()
        {
            RegisterCallback<MouseMoveEvent>(OnMouseMove, TrickleDown.TrickleDown);
            Undo.undoRedoPerformed = OnUndoRedoPerformed;
        }

        private void UnregisterCallbacks()
        {
            UnregisterCallback<MouseMoveEvent>(OnMouseMove, TrickleDown.TrickleDown);
            Undo.undoRedoPerformed = null;
        }

        private void CreateSearchWindowProvider()
        {
            _searchWindowProvider = ScriptableObject.CreateInstance<SearchWindowProvider>();
            _searchWindowProvider.Initialize(Graph);
            _searchWindowProvider.EntrySelected = OnSelectEntry;
        }

        private void CreateGraphView()
        {
            var edgeConnectorListener = new EdgeConnectorListener
            {
                Dropped = OnEdgeDrop,
                DroppedOutsidePort = OnEdgeDropOutsidePort
            };

            GraphView = new ExtendedGraphView(Graph, edgeConnectorListener)
            {
                NodeChanged = OnNodeChange,
                graphViewChanged = OnGraphViewChange,
                nodeCreationRequest = OnNodeCreateRequest,
                serializeGraphElements = OnSerializeGraphElements,
                canPasteSerializedData = GraphDataSerializer.CanPasteSerializedData,
                unserializeAndPaste = OnUnserializeAndPaste,
            };
            GraphViewBuilder.SyncViewToData(GraphView, Graph);

            var content = new VisualElement { name = "content" };
            Add(content);
            content.Add(GraphView);
        }

        private void OnMouseMove(MouseMoveEvent e)
        {
            _contextMousePos = GraphView.contentViewContainer.WorldToLocal(e.originalMousePosition);
        }

        private void OnNodeCreateRequest(NodeCreationContext context)
        {
            var searchContext = new SearchWindowContext(context.screenMousePosition);
            SearchWindow.Open(searchContext, _searchWindowProvider);
        }

        private void OnSelectEntry(Type nodeType, Vector2 position)
        {
            GraphDataBuilder.AddNode(Graph, FromScreenToGraphCoordinates(position), nodeType);
            GraphChanged();
        }

        private void OnEdgeDrop(Edge edgeView)
        {
            GraphDataBuilder.AddEdge(Graph, edgeView);
            GraphChanged();
        }

        private void OnEdgeDropOutsidePort(Edge edgeView, Vector2 _)
        {
            GraphDataBuilder.RemoveEdge(Graph, edgeView);
            GraphChanged();
        }

        private void OnUndoRedoPerformed()
        {
            Graph.MarkDirty();
            GraphChanged();
        }

        private void OnNodeChange(NodeView nodeView)
        {
            OnGraphChange();
        }

        private GraphViewChange OnGraphViewChange(GraphViewChange change)
        {
            if (change.movedElements != null)
            {
                GraphDataBuilder.SyncNodeDataToView(Graph, change.movedElements.OfType<NodeView>());
            }

            if (change.elementsToRemove != null)
            {
                GraphDataBuilder.RemoveNodes(Graph, change.elementsToRemove.OfType<NodeView>());
                GraphDataBuilder.RemoveEdges(Graph, change.elementsToRemove.OfType<Edge>());
                GraphChanged();
            }

            return change;
        }

        private string OnSerializeGraphElements(IEnumerable<GraphElement> elements)
        {
            return GraphDataSerializer.SerializeGraphElements(_contextMousePos, elements);
        }

        private void OnUnserializeAndPaste(string operationName, string data)
        {
            var deserializedData = GraphDataSerializer.DeserializeAndPaste(data);

            foreach (var node in deserializedData.Nodes)
            {
                var position = node.ViewData.Position + (_contextMousePos - deserializedData.CopyPosition);

                // When duplicating, avoid inserting the nodes on top of the original ones
                if (operationName == "Duplicate")
                {
                    position += new Vector2(32, 32);
                }

                GraphDataBuilder.AddNode(Graph, position, node);
            }

            foreach (var edge in deserializedData.Edges)
            {
                GraphDataBuilder.AddEdge(Graph, edge);
            }

            GraphChanged();
        }

        private void GraphChanged()
        {
            GraphViewBuilder.SyncViewToData(GraphView, Graph);
            OnGraphChange();
        }

        private Vector2 FromScreenToGraphCoordinates(Vector2 position)
        {
            var root = _editorWindow.rootVisualElement;
            var windowMousePosition = root.ChangeCoordinatesTo(root.parent, position - _editorWindow.position.position);
            return GraphView.contentViewContainer.WorldToLocal(windowMousePosition);
        }
    }
}
