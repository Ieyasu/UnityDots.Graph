using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace UnityDots.Graph
{
    public sealed class EdgeConnectorListener : IEdgeConnectorListener
    {
        public Action<Edge> Dropped { get; set; }
        public Action<Edge, Vector2> DroppedOutsidePort { get; set; }

        public void OnDrop(GraphView graphView, Edge edge)
        {
            Dropped?.Invoke(edge);
        }

        public void OnDropOutsidePort(Edge edge, Vector2 position)
        {
            DroppedOutsidePort?.Invoke(edge, position);
        }
    }
}
