using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Helper class for building the 'Create Node' context window.
    /// </summary>
    public sealed class SearchWindowProvider : ScriptableObject, ISearchWindowProvider
    {
        private struct NodeEntry
        {
            public string[] Title;
            public Type NodeType;
        }

        private Type _graphType;
        private Texture2D _icon;
        private List<SearchTreeEntry> _searchTree;
        public Action<Type, Vector2> EntrySelected;

        public void Initialize(GraphData graph)
        {
            // Transparent icon to trick search window into indenting items
            _icon = new Texture2D(1, 1);
            _icon.SetPixel(0, 0, new Color(0, 0, 0, 0));
            _icon.Apply();

            _graphType = graph.GetType();
            _searchTree = CreateSearchTree();
        }

        private void OnDestroy()
        {
            if (_icon != null)
            {
                DestroyImmediate(_icon);
                _icon = null;
            }
        }

        public bool OnSelectEntry(SearchTreeEntry entry, SearchWindowContext context)
        {
            var nodeEntry = (NodeEntry)entry.userData;
            EntrySelected?.Invoke(nodeEntry.NodeType, context.screenMousePosition);
            return true;
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext _)
        {
            return _searchTree;
        }

        private List<SearchTreeEntry> CreateSearchTree()
        {
            // First build up temporary data structure containing group & title as an array of strings (the last one is the actual title) and associated node type.
            var nodeEntries = new List<NodeEntry>();
            var types = TypeExtensions.GetAllTypes().Where(x => x.IsImplementationOf(typeof(NodeData)));

            foreach (var type in types)
            {
                var attribute = type.GetCustomAttribute(typeof(NodeAttribute), false);
                if (attribute is NodeAttribute nodeAttribute && nodeAttribute.GraphType == _graphType)
                {
                    nodeEntries.Add(new NodeEntry
                    {
                        Title = nodeAttribute.Title,
                        NodeType = type
                    });
                }
            }

            // Sort the entries lexicographically by group then title with the requirement that items always comes before sub-groups in the same group.
            // Example result:
            // - Art/BlendMode
            // - Art/Adjustments/ColorBalance
            // - Art/Adjustments/Contrast
            nodeEntries.Sort((entry1, entry2) =>
                {
                    for (var i = 0; i < entry1.Title.Length; i++)
                    {
                        if (i >= entry2.Title.Length)
                        {
                            return 1;
                        }

                        var value = entry1.Title[i].CompareTo(entry2.Title[i]);
                        if (value != 0)
                        {
                            // Make sure that leaves go before nodes
                            return entry1.Title.Length != entry2.Title.Length && (i == entry1.Title.Length - 1 || i == entry2.Title.Length - 1)
                                        ? entry1.Title.Length < entry2.Title.Length ? -1 : 1
                                        : value;
                        }
                    }
                    return 0;
                });

            // Build up the data structure needed by SearchWindow.

            // `groups` contains the current group path we're in.
            var groups = new List<string>();

            // First item in the tree is the title of the window.
            var tree = new List<SearchTreeEntry>
            {
                new SearchTreeGroupEntry(new GUIContent("Create Node"), 0),
            };

            foreach (var nodeEntry in nodeEntries)
            {
                // `createIndex` represents from where we should add new group entries from the current entry's group path.
                var createIndex = int.MaxValue;

                // Compare the group path of the current entry to the current group path.
                for (var i = 0; i < nodeEntry.Title.Length - 1; i++)
                {
                    var group = nodeEntry.Title[i];
                    if (i >= groups.Count)
                    {
                        // The current group path matches a prefix of the current entry's group path, so we add the
                        // rest of the group path from the current entry.
                        createIndex = i;
                        break;
                    }
                    if (groups[i] != group)
                    {
                        // A prefix of the current group path matches a prefix of the current entry's group path,
                        // so we remove everyfrom from the point where it doesn't match anymore, and then add the rest
                        // of the group path from the current entry.
                        groups.RemoveRange(i, groups.Count - i);
                        createIndex = i;
                        break;
                    }
                }

                // Create new group entries as needed.
                // If we don't need to modify the group path, `createIndex` will be `int.MaxValue` and thus the loop won't run.
                for (var i = createIndex; i < nodeEntry.Title.Length - 1; i++)
                {
                    var group = nodeEntry.Title[i];
                    groups.Add(group);
                    tree.Add(new SearchTreeGroupEntry(new GUIContent(group)) { level = i + 1 });
                }

                // Finally, add the actual entry.
                tree.Add(new SearchTreeEntry(new GUIContent(nodeEntry.Title.Last(), _icon))
                {
                    level = nodeEntry.Title.Length,
                    userData = nodeEntry
                });
            }

            return tree;
        }
    }
}
