using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnityDots.Graph.Editors
{
    public sealed class NodeViewMetadata
    {
        private static readonly Dictionary<Type, NodeViewMetadata> _metadata = new Dictionary<Type, NodeViewMetadata>();

        public List<ControlViewMetadata> ControlMetadata { get; }

        public string DisplayName { get; private set; }

        private NodeViewMetadata(Type nodeType)
        {
            ControlMetadata = new List<ControlViewMetadata>();

            SetDisplayName(nodeType);
            AddControlMetadata(nodeType);

            ControlMetadata.Sort();
        }

        public static NodeViewMetadata GetMetadata(Type nodeType)
        {
            if (!_metadata.TryGetValue(nodeType, out NodeViewMetadata metadata))
            {
                metadata = new NodeViewMetadata(nodeType);
                _metadata.Add(nodeType, metadata);
            }

            return metadata;
        }

        private void SetDisplayName(Type nodeType)
        {
            var attribute = nodeType.GetCustomAttribute<NodeAttribute>(false);
            if (!string.IsNullOrWhiteSpace(attribute.Name))
            {
                DisplayName = attribute.Name;
            }
            else
            {
                DisplayName = nodeType.Name.VariableNameToReadable();
            }
        }

        private void AddControlMetadata(Type nodeType)
        {
            var bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var fields = nodeType.GetFieldsRecursive(bindingFlags);

            foreach (var field in fields)
            {
                var controlAttribute = field.GetCustomAttribute<ControlAttribute>();
                if (controlAttribute != null)
                {
                    AddControlMetadata(field, controlAttribute);
                    continue;
                }
            }
        }

        private void AddControlMetadata(FieldInfo field, ControlAttribute attribute)
        {
            var label = attribute.Label ?? field.Name.VariableNameToReadable();
            ControlMetadata.Add(new ControlViewMetadata(field, label, attribute.Order));
        }
    }
}
