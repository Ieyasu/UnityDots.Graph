using System;
using System.Reflection;

namespace UnityDots.Graph.Editors
{
    public struct ControlViewMetadata : IComparable<ControlViewMetadata>
    {
        public FieldInfo Field { get; }
        public string Label { get; }
        public int Order { get; }

        public ControlViewMetadata(FieldInfo field, string label, int order)
        {
            Field = field;
            Label = label;
            Order = order;
        }

        public int CompareTo(ControlViewMetadata other)
        {
            var compare = Order.CompareTo(other.Order);
            if (compare != 0)
            {
                return compare;
            }

            if (Label != null)
            {
                compare = Label.CompareTo(other.Label);
            }
            else if (other.Label != null)
            {
                compare = -1;
            }
            if (compare != 0)
            {
                return compare;
            }

            if (Field != null)
            {
                compare = Field.Name.CompareTo(other.Field.Name);
            }
            else if (other.Field != null)
            {
                compare = -1;
            }

            return compare;
        }
    }
}
