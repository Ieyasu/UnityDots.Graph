using System;
using UnityDots.Editor;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Graph
{
    public sealed class NodePreview : VisualElement
    {
        private bool _isExpanded;
        private Image _preview;

        public Action<bool> Toggled
        {
            get;
            set;
        }

        public Texture Image
        {
            get => _preview.image;
            set
            {
                _preview.image = value;

                if (value == null)
                {
                    AddToClassList("hidden");
                }
                else
                {
                    RemoveFromClassList("hidden");
                }
            }
        }

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (_isExpanded != value)
                {
                    _isExpanded = value;
                    Toggled?.Invoke(value);
                }

                if (value)
                {
                    AddToClassList("expanded");
                    RemoveFromClassList("collapsed");
                }
                else
                {
                    AddToClassList("collapsed");
                    RemoveFromClassList("expanded");
                }

            }
        }

        public NodePreview(bool expanded)
        {
            this.LoadAndAddStyleSheet("Styles/NodePreview");
            AddToClassList("unitydots-node-preview");

            BuildPreview();
            IsExpanded = expanded;
            Image = null;
        }

        private void BuildPreview()
        {
            var collapsePreviewButton = new VisualElement { name = "collapse" };
            var collapseIcon = new VisualElement { name = "icon" };
            collapsePreviewButton.Add(collapseIcon);

            var expandPreviewButton = new VisualElement { name = "expand" };
            var expandIcon = new VisualElement { name = "icon" };
            expandPreviewButton.Add(expandIcon);

            _preview = new Image
            {
                name = "preview",
                tintColor = Color.white,
            };
            _preview.Add(collapsePreviewButton);

            var previewFiller = new VisualElement { name = "preview-filler" };
            previewFiller.Add(expandPreviewButton);

            Add(_preview);
            Add(previewFiller);

            collapsePreviewButton.AddManipulator(new Clickable(() =>
            {
                IsExpanded = false;
            }));

            expandPreviewButton.AddManipulator(new Clickable(() =>
            {
                IsExpanded = true;
            }));
        }
    }
}
