using System;
using System.Collections.Generic;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    public sealed class NodeSocketsView : VisualElement
    {
        private static readonly List<int> _socketsToDelete = new List<int>();

        private VisualElement _buttons;
        private readonly NodeView _nodeView;
        private readonly VisualElement _container;
        private readonly Dictionary<int, VisualElement> _elements;

        private bool IsInput => _container == _nodeView.inputContainer;
        public Dictionary<int, PortView> Sockets { get; } = new Dictionary<int, PortView>();

        public NodeSocketsView(NodeView nodeView, VisualElement container)
        {
            _nodeView = nodeView;
            _container = container;
            _elements = new Dictionary<int, VisualElement>();
        }

        public void Build()
        {
            SyncViewToData();

            if (IsInput && _nodeView.Node is IMultiInputNode multiInputNode)
            {
                BuildSocketListButtons(
                    multiInputNode.OnInputSocketAdd,
                    multiInputNode.OnInputSocketRemove,
                    multiInputNode.CanAddInputSocket,
                    multiInputNode.CanRemoveInputSocket);
            }

            if (!IsInput && _nodeView.Node is IMultiOutputNode multiOutputNode)
            {
                BuildSocketListButtons(
                    multiOutputNode.OnOutputSocketAdd,
                    multiOutputNode.OnOutputSocketRemove,
                    multiOutputNode.CanAddOutputSocket,
                    multiOutputNode.CanRemoveOutputSocket);
            }
        }

        public void SyncViewToData()
        {
            // Needed so sockets added later bind properly
            _nodeView.SerializedObject.Update();

            // Delete sockets that do not exist anymore
            _socketsToDelete.Clear();
            foreach (var id in Sockets.Keys)
            {
                var hasSocket = IsInput ? _nodeView.Node.HasInputSocket(id) : _nodeView.Node.HasOutputSocket(id);
                if (!hasSocket)
                {
                    _socketsToDelete.Add(id);
                }
            }
            foreach (var id in _socketsToDelete)
            {
                _container.Remove(_elements[id]);
                Sockets.Remove(id);
                _elements.Remove(id);
            }

            // Add sockets
            var sockets = IsInput ? _nodeView.Node.InputSockets : _nodeView.Node.OutputSockets;
            var direction = IsInput ? Direction.Input : Direction.Output;
            foreach (var socket in sockets)
            {
                // If socket exists already, don't readd it
                if (Sockets.ContainsKey(socket.ID))
                {
                    continue;
                }

                // Create the socket view
                var socketView = new PortView(socket.ID, _nodeView, Orientation.Horizontal, direction, socket.Capacity, socket.Type)
                {
                    portName = null
                };
                socketView.InitializeEdgeConnector(_nodeView.EdgeConnector);
                Sockets.Add(socketView.ID, socketView);

                // Either add the socket view straight to the container or add the associated control with it if found
                var index = _buttons != null ? _container.IndexOf(_buttons) : _container.childCount;
                if (!string.IsNullOrWhiteSpace(socket.ControlPropertyPath))
                {
                    socketView.AddToClassList("empty-label");
                    var slot = BuildSocketContainer(socketView, socket.Name, socket.ControlPropertyPath);
                    _container.Insert(index, slot);
                    _elements.Add(socketView.ID, slot);
                }
                else
                {
                    socketView.portName = socket.Name;
                    _container.Insert(index, socketView);
                    _elements.Add(socketView.ID, socketView);
                }
            }

            // Needed so sockets added later display properly
            _nodeView.Bind(_nodeView.SerializedObject);
        }

        private void BuildSocketListButtons(Action onAdd, Action onRemove, Func<bool> canAdd, Func<bool> canRemove)
        {
            _buttons = new VisualElement();

            var addButton = new Button(() =>
            {
                if (canAdd.Invoke())
                {
                    Undo.RecordObject(_nodeView.Node, "Add Socket");
                    onAdd.Invoke();
                    SyncViewToData();
                    _nodeView.OnNodeChanged();
                }
            });

            var removeButton = new Button(() =>
            {
                if (canRemove.Invoke())
                {
                    Undo.RecordObject(_nodeView.Node, "Remove Socket");
                    onRemove.Invoke();
                    SyncViewToData();
                    _nodeView.OnNodeChanged();
                }
            });


            addButton.AddToClassList("unitydots-node-view--button");
            addButton.AddToClassList("unitydots-node-view--button__add");
            removeButton.AddToClassList("unitydots-node-view--button");
            removeButton.AddToClassList("unitydots-node-view--button__remove");
            _buttons.AddToClassList("unitydots-node-view--buttons");
            _buttons.Add(addButton);
            _buttons.Add(removeButton);
            _container.Add(_buttons);
        }

        private VisualElement BuildSocketContainer(PortView socket, string name, string propertyPath)
        {
            var property = _nodeView.SerializedObject.FindProperty(propertyPath);
            var control = new PropertyField(property)
            {
                name = "socket-control"
            };

            control.style.flexGrow = 1;
            control.style.marginLeft = -4;
            control.style.marginTop = 2;
            control.label = name;

            var socketContainer = new VisualElement
            {
                name = "socket-container"
            };
            socketContainer.Add(socket);
            socketContainer.Add(control);
            control.RegisterToAllChanges(_nodeView.OnNodeChanged);
            return socketContainer;
        }
    }
}
