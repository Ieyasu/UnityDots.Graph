using System;
using UnityDots.Editor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Implementation of Port.
    /// </summary>
    public sealed class PortView : Port
    {
        public int ID { get; }

        public NodeView NodeView { get; }

        public PortView(int id, NodeView nodeView, Orientation orientation, Direction direction, int capacity, Type type)
            : base(orientation, direction, GetCapacity(capacity), type)
        {
            this.LoadAndAddStyleSheet("Styles/PortView");
            AddToClassList("unitydots-port-view");

            ID = id;
            NodeView = nodeView;
        }

        public void InitializeEdgeConnector(IEdgeConnectorListener connectorListener)
        {
            m_EdgeConnector = new EdgeConnector<Edge>(connectorListener);
            this.AddManipulator(m_EdgeConnector);
        }

        private static Capacity GetCapacity(int capacity)
        {
            return capacity == 1 ? Capacity.Single : Capacity.Multi;
        }
    }
}
