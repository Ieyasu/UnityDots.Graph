using System;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Graph.Editors
{
    /// <summary>
    /// Visual element for representing a node in the graph view.
    /// </summary>
    public class NodeView : Node
    {
        private NodeSocketsView _inputSockets;
        private NodeSocketsView _outputSockets;

        public NodeData Node => userData as NodeData;
        public Action NodeChanged { get; set; }
        public bool HasPreview => Preview.Image != null;
        public NodePreview Preview { get; private set; }

        internal SerializedObject SerializedObject { get; private set; }
        internal IEdgeConnectorListener EdgeConnector { get; private set; }

        public override bool expanded
        {
            get => base.expanded;
            set
            {
                if (expanded != value)
                {
                    base.expanded = value;
                    RefreshExpandedState();
                    OnNodeChanged();
                }
            }
        }

        public void Initialize(NodeData node, IEdgeConnectorListener edgeConnector)
        {
            this.LoadAndAddStyleSheet("Styles/NodeView");
            AddToClassList("unitydots-node-view");

            userData = node;
            var metadata = NodeViewMetadata.GetMetadata(node.GetType());
            title = name = metadata.DisplayName;

            var contents = this.Q("contents");
            SerializedObject = new SerializedObject(Node);
            contents.RegisterToAllChanges(OnNodeChanged);

            EdgeConnector = edgeConnector;
            BuildView(contents, SerializedObject);
            SyncViewToData();

            this.Bind(SerializedObject);
        }

        public void SyncViewToData()
        {
            var position = new Rect(Node.ViewData.Position.x, Node.ViewData.Position.y, 0, 0);
            SetPosition(position);
            expanded = Node.ViewData.IsExpanded;
            Preview.IsExpanded = Node.ViewData.IsPreviewExpanded;
            _inputSockets.SyncViewToData();
            _outputSockets.SyncViewToData();
        }

        public PortView GetInputSocket(int socketID)
        {
            return _inputSockets.Sockets[socketID];
        }

        public PortView GetOutputSocket(int socketID)
        {
            return _outputSockets.Sockets[socketID];
        }

        public void OnNodeChanged()
        {
            NodeChanged?.Invoke();
        }

        protected virtual void BuildView(VisualElement contents, SerializedObject serializedObject)
        {
            BuildDefaultView(contents, serializedObject);
        }

        protected virtual void BuildControls(VisualElement controls, SerializedObject serializedObject)
        {
            var metadata = NodeViewMetadata.GetMetadata(Node.GetType());
            if (metadata.ControlMetadata.Count == 0)
            {
                return;
            }

            foreach (var control in metadata.ControlMetadata)
            {
                var serializedProperty = serializedObject.FindProperty(control.Field.Name);
                var field = new PropertyField(serializedProperty, control.Label);
                controls.Add(field);
            }
        }

        protected void BuildDefaultView(VisualElement contents, SerializedObject serializedObject)
        {
            BuildInputSockets(inputContainer);
            BuildOutputSockets(outputContainer);
            BuildControlsInternal(contents, serializedObject);
            BuildPreview(contents);
        }

        protected void BuildInputSockets(VisualElement container)
        {
            _inputSockets = new NodeSocketsView(this, container);
            _inputSockets.Build();
        }

        protected void BuildOutputSockets(VisualElement container)
        {
            _outputSockets = new NodeSocketsView(this, container);
            _outputSockets.Build();
        }

        protected void BuildPreview(VisualElement container)
        {
            var previewDivider = new VisualElement { name = "divider" };
            previewDivider.AddToClassList("horizontal");
            container.Add(previewDivider);

            Preview = new NodePreview(Node.ViewData.IsPreviewExpanded)
            {
                Toggled = expanded => OnNodeChanged()
            };

            container.Add(Preview);
        }

        private void BuildControlsInternal(VisualElement container, SerializedObject serializedObject)
        {
            var controls = new VisualElement { name = "items" };
            BuildControls(controls, serializedObject);

            var controlsContents = new VisualElement { name = "controls" };
            var controlsDivider = new VisualElement { name = "divider" };
            controlsDivider.AddToClassList("horizontal");
            controlsContents.Add(controlsDivider);
            controlsContents.Add(controls);
            container.Add(controlsContents);
        }
    }
}
