using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Graph
{
    [Serializable]
    public struct EdgeConnection : IEquatable<EdgeConnection>
    {
        [SerializeField]
        private NodeData _node;
        public NodeData Node => _node;

        [SerializeField]
        private int _socketID;
        public int SocketID => _socketID;

        public EdgeConnection(NodeData node, int socketID)
        {
            _node = node;
            _socketID = socketID;
        }

        public override bool Equals(object other)
        {
            return other is EdgeConnection otherConnection && Equals(otherConnection);
        }

        public bool Equals(EdgeConnection other)
        {
            return Node == other.Node && SocketID == other.SocketID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Node, SocketID.GetHashCode());
        }

        public override string ToString()
        {
            return $"EdgeConnection(Node = {Node}, SocketID = {SocketID})";
        }
    }

    [Serializable]
    public struct EdgeData : IEquatable<EdgeData>
    {
        [SerializeField]
        private EdgeConnection _start;
        public EdgeConnection Start => _start;

        [SerializeField]
        private EdgeConnection _end;
        public EdgeConnection End => _end;

        public EdgeData(EdgeConnection start, EdgeConnection end)
        {
            _start = start;
            _end = end;
        }

        public EdgeData(NodeData startNode, int startID, NodeData endNode, int endID)
        {
            _start = new EdgeConnection(startNode, startID);
            _end = new EdgeConnection(endNode, endID);
        }

        public override bool Equals(object other)
        {
            return other is EdgeData edge && Equals(edge);
        }

        public bool Equals(EdgeData other)
        {
            return Start.Equals(other.Start) && End.Equals(other.End);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_start, _end);
        }

        public override string ToString()
        {
            return $@"EdgeData(
                Start Node = {_start.Node}, Start Socket ID = {_start.SocketID}),
                End Node = {_end.Node}, End Socket ID = {_end.SocketID})";
        }
    }
}
