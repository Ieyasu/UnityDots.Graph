using System;
using UnityEngine;

namespace UnityDots.Graph
{
    [Serializable]
    public struct NodeViewData : IEquatable<NodeViewData>
    {
        [SerializeField]
        private bool _isExpanded;
        public bool IsExpanded
        {
            get => _isExpanded;
            set => _isExpanded = value;
        }

        [SerializeField]
        private bool _isPreviewExpanded;
        public bool IsPreviewExpanded
        {
            get => _isPreviewExpanded;
            set => _isPreviewExpanded = value;
        }

        [SerializeField]
        private Vector2 _position;
        public Vector2 Position
        {
            get => _position;
            set => _position = value;
        }

        public NodeViewData(bool expanded)
        {
            _isExpanded = expanded;
            _isPreviewExpanded = true;
            _position = new Vector2();
        }

        public override bool Equals(object obj)
        {
            return obj is NodeViewData other && Equals(other);
        }

        public bool Equals(NodeViewData other)
        {
            return IsExpanded == other.IsExpanded
                    && IsPreviewExpanded == other.IsPreviewExpanded
                    && Position == other.Position;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(IsExpanded, IsPreviewExpanded, Position);
        }
    }
}
