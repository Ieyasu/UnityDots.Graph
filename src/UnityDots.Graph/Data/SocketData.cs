using System;

namespace UnityDots.Graph
{
    public abstract class SocketData
    {
        public int ID { get; set; }
        public int Capacity { get; set; }
        public string Name { get; set; }
        public string ControlPropertyPath { get; set; }

        public abstract Type Type { get; }
        public abstract bool HasValue { get; }

        public abstract void Clear();

        public abstract object GetValue();
        public abstract void SetValue(object value);
    }

    public abstract class SocketData<T> : SocketData
    {
        private T _value;
        private bool _hasValue;

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException("Socket has no value");
                }
                return _value;
            }
            set
            {
                _value = value;
                _hasValue = true;
            }
        }

        public override Type Type => typeof(T);
        public override bool HasValue => _hasValue;
        protected T RawValue => _value;

        public override void Clear()
        {
            Value = default;
            _hasValue = false;
        }

        public override object GetValue()
        {
            return Value;
        }

        public override void SetValue(object value)
        {
            Value = (T)value;
        }
    }

    public sealed class DefaultSocket : SocketData<int>
    {
    }
}
