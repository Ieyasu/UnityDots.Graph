using System;

namespace UnityDots.Graph
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class InputAttribute : Attribute
    {
        public int ID { get; set; }
        public int Order { get; set; }
        public int Capacity { get; set; }
        public string Label { get; set; }

        public InputAttribute(int id, string label = null, int order = int.MaxValue, int capacity = 1)
        {
            ID = id;
            Label = label;
            Order = order;
            Capacity = capacity;
        }
    }
}
