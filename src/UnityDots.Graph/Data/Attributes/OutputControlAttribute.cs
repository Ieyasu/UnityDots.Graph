using System;

namespace UnityDots.Graph
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class OutputControlAttribute : Attribute
    {
        public int ID;

        public OutputControlAttribute(int id)
        {
            ID = id;
        }
    }
}
