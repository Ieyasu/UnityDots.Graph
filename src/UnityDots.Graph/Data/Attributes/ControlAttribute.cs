using System;

namespace UnityDots.Graph
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class ControlAttribute : Attribute
    {
        public int Order;
        public string Label;

        public ControlAttribute(string label = null, int order = int.MaxValue)
        {
            Order = order;
            Label = label;
        }
    }
}
