using System;

namespace UnityDots.Graph
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class NodeAttribute : Attribute
    {
        public Type GraphType { get; }
        public string[] Title { get; }

        // Named parameters
        public string Name { get; set; }

        public NodeAttribute(Type graphType, string path)
        {
            GraphType = graphType;
            Title = path.Split('/');
            Name = Title.Length > 0 ? Title[Title.Length - 1] : string.Empty;
        }
    }
}
