using System;

namespace UnityDots.Graph
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class InputControlAttribute : Attribute
    {
        public int ID;

        public InputControlAttribute(int id)
        {
            ID = id;
        }
    }
}
