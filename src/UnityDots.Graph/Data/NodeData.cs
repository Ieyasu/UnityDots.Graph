using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Graph
{
    public abstract class NodeData : ScriptableObject
    {
        public GraphData Graph { get; internal set; }
        public List<SocketData> InputSockets { get; private set; }
        public List<SocketData> OutputSockets { get; private set; }

        [SerializeField]
        private NodeViewData _viewData = new NodeViewData(true);
        public NodeViewData ViewData
        {
            get => _viewData;
            set => _viewData = value;
        }

        protected virtual void OnEnable()
        {
            BuildSocketsInternal();
        }

        protected virtual void BuildSockets() { }

        public void ResetSockets()
        {
            foreach (var socket in OutputSockets)
            {
                socket.Clear();
            }

            foreach (var socket in InputSockets)
            {
                socket.Clear();
            }
        }

        public bool HasInputSocket(int socketID)
        {
            return GetInputSocket(socketID) != null;
        }

        public bool HasOutputSocket(int socketID)
        {
            return GetOutputSocket(socketID) != null;
        }

        public SocketData GetInputSocket(int socketID)
        {
            return InputSockets.Find(x => x.ID == socketID);
        }

        public SocketData GetOutputSocket(int socketID)
        {
            return OutputSockets.Find(x => x.ID == socketID);
        }

        public void AddInputSocket(SocketData socket)
        {
            if (HasInputSocket(socket.ID))
            {
                throw new ArgumentException($"Input socket with ID {socket.ID} already exists");
            }
            InputSockets.Add(socket);
        }

        public void AddOutputSocket(SocketData socket)
        {
            if (HasOutputSocket(socket.ID))
            {
                throw new ArgumentException($"Output socket with ID {socket.ID} already exists");
            }
            OutputSockets.Add(socket);
        }

        public void RemoveInputSocket(int socketID)
        {
            InputSockets.RemoveAll(x => x.ID == socketID);
        }

        public void RemoveOutputSocket(int socketID)
        {
            OutputSockets.RemoveAll(x => x.ID == socketID);
        }

        private void BuildSocketsInternal()
        {
            if (InputSockets != null && OutputSockets != null)
            {
                return;
            }

            InputSockets = new List<SocketData>();
            OutputSockets = new List<SocketData>();

            var nodeMetadata = NodeMetadata.Get(this);
            foreach (var socketMetadata in nodeMetadata.InputSocketMetadata)
            {
                var socket = nodeMetadata.CreateSocket(this, socketMetadata);
                AddInputSocket(socket);
            }

            foreach (var socketMetadata in nodeMetadata.OutputSocketMetadata)
            {
                var socket = nodeMetadata.CreateSocket(this, socketMetadata);
                AddOutputSocket(socket);
            }

            BuildSockets();
        }
    }
}
