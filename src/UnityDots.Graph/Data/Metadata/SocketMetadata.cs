using System.Reflection;

namespace UnityDots.Graph
{
    internal struct SocketMetadata
    {
        public int ID { get; }
        public int Capacity { get; }
        public string Label { get; }
        public PropertyInfo Property { get; }
        public string ControlPropertyPath { get; }

        public SocketMetadata(int id, int capacity, string label, PropertyInfo property, string controlPropertyPath)
        {
            ID = id;
            Capacity = capacity;
            Label = label;
            Property = property;
            ControlPropertyPath = controlPropertyPath;
        }
    }
}
