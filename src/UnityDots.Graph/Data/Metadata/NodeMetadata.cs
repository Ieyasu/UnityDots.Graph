using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnityDots.Graph
{
    internal sealed class NodeMetadata
    {
        private static readonly Dictionary<Type, NodeMetadata> _maps = new Dictionary<Type, NodeMetadata>();

        public List<SocketMetadata> InputSocketMetadata { get; }
        public List<SocketMetadata> OutputSocketMetadata { get; }

        private NodeMetadata(Type nodeType)
        {
            InputSocketMetadata = new List<SocketMetadata>();
            OutputSocketMetadata = new List<SocketMetadata>();
            AddSocketInfo(nodeType);
        }

        public static NodeMetadata Get(NodeData node)
        {
            var nodeType = node.GetType();
            if (!_maps.TryGetValue(nodeType, out NodeMetadata map))
            {
                map = new NodeMetadata(nodeType);
                _maps.Add(nodeType, map);
            }

            return map;
        }

        public bool HasInput(int id)
        {
            return InputSocketMetadata.Any(x => x.ID == id);
        }

        public bool HasOutput(int id)
        {
            return OutputSocketMetadata.Any(x => x.ID == id);
        }

        public SocketMetadata GetInput(int id)
        {
            return InputSocketMetadata.Find(x => x.ID == id);
        }

        public SocketMetadata GetOutput(int id)
        {
            return OutputSocketMetadata.Find(x => x.ID == id);
        }

        public SocketData CreateSocket(NodeData node, SocketMetadata socketMetadata)
        {
            var socket = Activator.CreateInstance(socketMetadata.Property.PropertyType) as SocketData;
            socket.ID = socketMetadata.ID;
            socket.Capacity = socketMetadata.Capacity;
            socket.Name = socketMetadata.Label;
            socket.ControlPropertyPath = socketMetadata.ControlPropertyPath;
            socketMetadata.Property.SetValue(node, socket);
            return socket;
        }

        private void AddSocketInfo(Type nodeType)
        {
            var bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var properties = nodeType.GetPropertiesRecursive(bindingFlags);
            var fields = nodeType.GetFieldsRecursive(bindingFlags);

            foreach (var property in properties)
            {
                if (property.PropertyType.IsSubclassOf(typeof(SocketData)))
                {
                    var inputAttr = property.GetCustomAttribute<InputAttribute>();
                    if (inputAttr != null && !property.SetMethod.IsAbstract)
                    {
                        var label = inputAttr.Label ?? property.Name.VariableNameToReadable();
                        var controlPropertyPath = GetInputControlPropertyPath(inputAttr.ID, fields);
                        var input = new SocketMetadata(inputAttr.ID, inputAttr.Capacity, label, property, controlPropertyPath);
                        InputSocketMetadata.Add(input);
                        continue;
                    }

                    var outputAttr = property.GetCustomAttribute<OutputAttribute>();
                    if (outputAttr != null && !property.GetMethod.IsAbstract)
                    {
                        var label = outputAttr.Label ?? property.Name.VariableNameToReadable();
                        var controlPropertyPath = GetOutputControlPropertyPath(outputAttr.ID, fields);
                        var output = new SocketMetadata(outputAttr.ID, outputAttr.Capacity, label, property, controlPropertyPath);
                        OutputSocketMetadata.Add(output);
                        continue;
                    }
                }
            }
        }

        private string GetInputControlPropertyPath(int socketID, IEnumerable<FieldInfo> fields)
        {
            foreach (var field in fields)
            {
                var attribute = field.GetCustomAttribute<InputControlAttribute>();
                if (attribute != null && attribute.ID == socketID)
                {
                    return field.Name;
                }
            }
            return null;
        }

        private string GetOutputControlPropertyPath(int socketID, IEnumerable<FieldInfo> fields)
        {
            foreach (var field in fields)
            {
                var attribute = field.GetCustomAttribute<OutputControlAttribute>();
                if (attribute != null && attribute.ID == socketID)
                {
                    return field.Name;
                }
            }
            return null;
        }
    }
}
