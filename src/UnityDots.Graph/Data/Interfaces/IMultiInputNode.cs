namespace UnityDots.Graph
{
    public interface IMultiInputNode
    {
        bool CanAddInputSocket();
        bool CanRemoveInputSocket();
        void OnInputSocketAdd();
        void OnInputSocketRemove();
    }
}
