namespace UnityDots.Graph
{
    public interface IMultiOutputNode
    {
        bool CanAddOutputSocket();
        bool CanRemoveOutputSocket();
        void OnOutputSocketAdd();
        void OnOutputSocketRemove();
    }
}
