using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace UnityDots.Graph
{
    public sealed class AdjacencyList
    {
        private sealed class EdgeMap
        {
            public NodeData Node { get; }
            public List<EdgeData> InputEdges { get; }
            public List<EdgeData> OutputEdges { get; }
            public ReadOnlyCollection<EdgeData> InputEdgesReadOnly { get; }
            public ReadOnlyCollection<EdgeData> OutputEdgesReadOnly { get; }

            public EdgeMap(NodeData node)
            {
                Node = node;
                InputEdges = new List<EdgeData>();
                OutputEdges = new List<EdgeData>();
                InputEdgesReadOnly = InputEdges.AsReadOnly();
                OutputEdgesReadOnly = OutputEdges.AsReadOnly();
            }
        }

        private readonly Dictionary<NodeData, EdgeMap> _adjacencyList;

        public AdjacencyList()
        {
            _adjacencyList = new Dictionary<NodeData, EdgeMap>();
        }

        public void Rebuild(List<NodeData> nodes, List<EdgeData> edges)
        {
            _adjacencyList.Clear();

            foreach (var node in nodes)
            {
                AddNode(node);
            }

            foreach (var edge in edges)
            {
                AddEdge(edge);
            }
        }

        public bool ContainsNode(NodeData node)
        {
            return _adjacencyList.ContainsKey(node);
        }

        public bool AddNode(NodeData node)
        {
            if (node == null)
            {
                return false;
            }

            _adjacencyList.Add(node, new EdgeMap(node));
            return true;
        }

        public bool AddEdge(EdgeData edge)
        {
            if (!ValidateOutputSocketCapacity(edge.Start.Node, edge.Start.SocketID)
             || !ValidateInputSocketCapacity(edge.End.Node, edge.End.SocketID))
            {
                return false;
            }

            _adjacencyList[edge.Start.Node].OutputEdges.Add(edge);
            _adjacencyList[edge.End.Node].InputEdges.Add(edge);
            return true;
        }

        public bool RemoveNode(NodeData node)
        {
            return _adjacencyList.Remove(node);
        }

        public bool RemoveEdge(EdgeData edge)
        {
            bool removed = false;
            if (edge.Start.Node != null && _adjacencyList.TryGetValue(edge.Start.Node, out EdgeMap outputMap))
            {
                outputMap.OutputEdges.Remove(edge);
                removed = true;
            }
            if (edge.End.Node != null && _adjacencyList.TryGetValue(edge.End.Node, out EdgeMap inputMap))
            {
                inputMap.InputEdges.Remove(edge);
                removed = true;
            }
            return removed;
        }

        public ReadOnlyCollection<EdgeData> GetInputEdges(NodeData node)
        {
            var edgeMap = GetEdgeMap(node);
            return edgeMap.InputEdgesReadOnly;
        }

        public ReadOnlyCollection<EdgeData> GetOutputEdges(NodeData node)
        {
            var edgeMap = GetEdgeMap(node);
            return edgeMap.OutputEdgesReadOnly;
        }

        public EdgeData[] GetInputEdges(NodeData node, int socketID)
        {
            return GetInputEdges(node).Where(x => x.End.SocketID == socketID).ToArray();
        }

        public EdgeData[] GetOutputEdges(NodeData node, int socketID)
        {
            return GetOutputEdges(node).Where(x => x.Start.SocketID == socketID).ToArray();
        }

        private EdgeMap GetEdgeMap(NodeData node)
        {
            if (node == null)
            {
                throw new ArgumentException("Failed to get output edges: node is null");
            }

            return _adjacencyList[node];
        }

        private bool ValidateInputSocketCapacity(NodeData node, int socketID)
        {
            if (node == null)
            {
                Debug.LogError("Can't add edge: end node is null");
                return false;
            }

            var socketEdgeCount = GetInputEdges(node, socketID).Count(x => x.End.SocketID == socketID);
            var socket = node.GetInputSocket(socketID);
            if (socket == null)
            {
                Debug.LogError($"Can't add edge: no input socket with id {socketID}");
                return false;
            }

            if (socket.Capacity >= 0 && socket.Capacity <= socketEdgeCount)
            {
                Debug.LogError($"Can't add edge: socket {socketID}'s capacity of {socket.Capacity} at maximum");
                return false;
            }

            return true;
        }

        private bool ValidateOutputSocketCapacity(NodeData node, int socketID)
        {
            if (node == null)
            {
                Debug.LogError("Can't add edge: start node is null");
                return false;
            }

            var socketEdgeCount = GetOutputEdges(node, socketID).Count(x => x.Start.SocketID == socketID);
            var socket = node.GetOutputSocket(socketID);
            if (socket == null)
            {
                Debug.LogError($"Can't add edge: no output socket with id {socketID}");
                return false;
            }

            if (socket.Capacity >= 0 && socket.Capacity <= socketEdgeCount)
            {
                Debug.LogError($"Can't add edge: socket {socketID}'s capacity of {socket.Capacity} at maximum");
                return false;
            }

            return true;
        }
    }
}
