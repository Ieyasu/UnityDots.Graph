using System;

namespace UnityDots.Graph
{
    public interface IEdgeConverter
    {
        bool CanConvert(Type outputType, Type inputType);
        object Convert(object output);
    }
}
