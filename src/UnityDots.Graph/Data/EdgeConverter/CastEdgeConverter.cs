using System;

namespace UnityDots.Graph
{
    public sealed class CastEdgeConverter : IEdgeConverter
    {
        public bool CanConvert(Type outputType, Type inputType)
        {
            return outputType.IsCastableTo(inputType);
        }

        public object Convert(object output)
        {
            return output;
        }
    }
}
