using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace UnityDots.Graph
{
    public abstract class GraphData : ScriptableObject
    {
        private bool _isDirty = true;
        private ReadOnlyCollection<NodeData> _nodesReadOnly;
        private ReadOnlyCollection<EdgeData> _edgesReadOnly;
        private ReadOnlyCollection<IEdgeConverter> _edgeConvertersReadOnly;

        private AdjacencyList _adjacencyList;
        private List<IEdgeConverter> _edgeConverters;

        [SerializeField]
        private List<NodeData> _nodes = new List<NodeData>();

        [SerializeField]
        private List<EdgeData> _edges = new List<EdgeData>();

        public ReadOnlyCollection<NodeData> Nodes
        {
            get
            {
                if (_nodes != null && _nodesReadOnly == null)
                {
                    // Initialize nodes, TODO a better place to do this
                    _nodesReadOnly = _nodes.AsReadOnly();
                }
                return _nodesReadOnly;
            }
        }

        public ReadOnlyCollection<EdgeData> Edges
        {
            get
            {
                if (_edges != null && _edgesReadOnly == null)
                {
                    _edgesReadOnly = _edges.AsReadOnly();
                }
                return _edgesReadOnly;
            }
        }

        public ReadOnlyCollection<IEdgeConverter> EdgeConverters
        {
            get
            {
                if (_isDirty || _edgeConvertersReadOnly == null)
                {
                    RebuildVolatileData();
                }
                return _edgeConvertersReadOnly;
            }
        }

        private AdjacencyList AdjacencyList
        {
            get
            {
                if (_isDirty || _adjacencyList == null)
                {
                    RebuildVolatileData();
                }
                return _adjacencyList;
            }
        }



        private void OnEnable()
        {
            // Hack required to fix issue with subassets when duplicating a main asset
            // https://issuetracker.unity3d.com/issues/parent-and-child-nested-scriptable-object-assets-switch-places-when-parent-scriptable-object-asset-is-renamed
            #if UNITY_EDITOR
            var assetPath = UnityEditor.AssetDatabase.GetAssetPath(this);
            var fileName = System.IO.Path.GetFileNameWithoutExtension(assetPath);
            name = fileName;
            #endif

            foreach (var node in _nodes)
            {
                if (node != null)
                {
                    node.Graph = this;
                }
            }
        }


        protected virtual void CreateEdgeConverters(List<IEdgeConverter> converters)
        {
            converters.Add(new CastEdgeConverter());
        }

        public void MarkDirty()
        {
            _isDirty = true;
        }

        public ReadOnlyCollection<EdgeData> GetInputEdges(NodeData node)
        {
            return AdjacencyList.GetInputEdges(node);
        }

        public ReadOnlyCollection<EdgeData> GetOutputEdges(NodeData node)
        {
            return AdjacencyList.GetOutputEdges(node);
        }

        public bool IsValidEdge(Type outputType, Type inputType)
        {
            return GetEdgeConverter(outputType, inputType) != null;
        }

        public IEdgeConverter GetEdgeConverter(Type outputType, Type inputType)
        {
            foreach (var converter in EdgeConverters)
            {
                if (converter.CanConvert(outputType, inputType))
                {
                    return converter;
                }
            }

            return null;
        }

        public bool ContainsNode(NodeData node)
        {
            return AdjacencyList.ContainsNode(node);
        }

        public void AddNode(NodeData node)
        {
            if (node == null)
            {
                Debug.LogError("Node is null");
                return;
            }

            if (AdjacencyList.ContainsNode(node))
            {
                Debug.LogWarning($"Graph already contains node {node}");
                return;
            }

            _nodes.Add(node);
            AdjacencyList.AddNode(node);
            node.Graph = this;
        }

        public bool AddEdge(EdgeData edge, bool replaceExisting = false)
        {
            if (_edges.Contains(edge))
            {
                Debug.LogWarning($"Graph already contains edge {edge}");
                return false;
            }

            if (replaceExisting)
            {
                RemoveExcessOutputEdges(edge.Start, 1);
                RemoveExcessInputEdges(edge.End, 1);
            }

            if (!AdjacencyList.AddEdge(edge))
            {
                return false;
            }

            _edges.Add(edge);
            return true;
        }

        public bool RemoveNode(NodeData node)
        {
            var index = _nodes.IndexOf(node);
            if (index == -1)
            {
                return false;
            }

            RemoveNodeAt(index);
            return true;
        }

        public void RemoveNodeAt(int index)
        {
            var node = _nodes[index];

            if (node != null)
            {
                RemoveInputEdges(node);
                RemoveOutputEdges(node);
                AdjacencyList.RemoveNode(node);
                node.Graph = null;
            }

            _nodes.RemoveAt(index);
        }

        public bool RemoveEdge(EdgeData edge)
        {
            var index = _edges.IndexOf(edge);
            if (index == -1)
            {
                return false;
            }

            RemoveEdgeAt(index);
            return true;
        }

        public void RemoveEdgeAt(int index)
        {
            var edge = _edges[index];

            AdjacencyList.RemoveEdge(edge);

            _edges.RemoveAt(index);
        }

        public void RemoveInputEdges(NodeData node)
        {
            var edges = AdjacencyList.GetInputEdges(node);
            for (var i = edges.Count - 1; i >= 0; --i)
            {
                RemoveEdge(edges[i]);
            }
        }

        public void RemoveOutputEdges(NodeData node)
        {
            var edges = AdjacencyList.GetOutputEdges(node);
            for (var i = edges.Count - 1; i >= 0; --i)
            {
                RemoveEdge(edges[i]);
            }
        }

        private void RebuildVolatileData()
        {
            if (_adjacencyList == null)
            {
                _adjacencyList = new AdjacencyList();
            }
            _adjacencyList.Rebuild(_nodes, _edges);

            if (_edgeConverters == null)
            {
                _edgeConverters = new List<IEdgeConverter>();
                _edgeConvertersReadOnly = _edgeConverters.AsReadOnly();
            }
            _edgeConverters.Clear();
            CreateEdgeConverters(_edgeConverters);

            _isDirty = false;
        }

        private void RemoveExcessInputEdges(EdgeConnection connection, int extraCapacity)
        {
            var socket = connection.Node.GetInputSocket(connection.SocketID);
            if (socket == null || socket.Capacity < 0)
            {
                return;
            }

            var edges = _adjacencyList.GetInputEdges(connection.Node, connection.SocketID);
            var target = Mathf.Max(socket.Capacity - extraCapacity, 0);
            for (var i = edges.Length - 1; i >= target; --i)
            {
                RemoveEdge(edges[i]);
            }
        }

        private void RemoveExcessOutputEdges(EdgeConnection connection, int extraCapacity)
        {
            var socket = connection.Node.GetOutputSocket(connection.SocketID);
            if (socket == null || socket.Capacity < 0)
            {
                return;
            }

            var edges = _adjacencyList.GetOutputEdges(connection.Node, connection.SocketID);
            var target = Mathf.Max(socket.Capacity - extraCapacity, 0);
            for (var i = edges.Length - 1; i >= target; --i)
            {
                RemoveEdge(edges[i]);
            }
        }
    }
}
