using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Graph
{
    public sealed class GraphEvaluator
    {
        public readonly List<NodeData> _nodes;
        public readonly Stack<NodeData> _nodeStack;
        public readonly HashSet<EdgeData> _processedEdges;

        public GraphEvaluator()
        {
            _nodes = new List<NodeData>();
            _nodeStack = new Stack<NodeData>();
            _processedEdges = new HashSet<EdgeData>();
        }

        public void ResetGraph(GraphData graph)
        {
            foreach (var node in graph.Nodes)
            {
                node.ResetSockets();
            }
        }

        public void EvaluateGraph(GraphData graph, Action<NodeData> onEvaluateNode, bool reset = true)
        {
            if (reset)
            {
                ResetGraph(graph);
            }

            _nodes.Clear();
            _nodeStack.Clear();
            _processedEdges.Clear();

            _nodes.AddRange(graph.Nodes);
            _nodes.Sort((node1, node2) => node1.name.CompareTo(node2.name));

            for (var i = _nodes.Count - 1; i >= 0; --i)
            {
                var node = _nodes[i];
                if (graph.GetInputEdges(node).Count == 0)
                {
                    _nodeStack.Push(node);
                }
            }

            while (_nodeStack.Count != 0)
            {
                var node = _nodeStack.Pop();
                EvaluateNode(graph, node, onEvaluateNode);
            }
        }

        private void EvaluateNode(GraphData graph, NodeData node, Action<NodeData> onEvaluateNode)
        {
            onEvaluateNode(node);

            var outputEdges = graph.GetOutputEdges(node);
            for (var i = outputEdges.Count - 1; i >= 0; --i)
            {
                var outputEdge = outputEdges[i];
                EvaluateEdge(graph, outputEdge);

                // Add nodes with all incoming edges processed.
                var addNode = true;
                foreach (var inputEdge in graph.GetInputEdges(outputEdge.End.Node))
                {
                    if (!_processedEdges.Contains(inputEdge))
                    {
                        addNode = false;
                        break;
                    }
                }

                if (addNode)
                {
                    _nodeStack.Push(outputEdge.End.Node);
                }
            }
        }

        private void EvaluateEdge(GraphData graph, EdgeData edge)
        {
            _processedEdges.Add(edge);

            var startSocket = edge.Start.Node.GetOutputSocket(edge.Start.SocketID);
            var endSocket = edge.End.Node.GetInputSocket(edge.End.SocketID);

            if (!startSocket.HasValue)
            {
                return;
            }

            var edgeConverter = graph.GetEdgeConverter(startSocket.Type, endSocket.Type);
            if (edgeConverter == null)
            {
                Debug.LogError($"Can not convert from '{startSocket.Type}' to '{endSocket.Type}'");
                return;
            }

            var value = startSocket.GetValue();
            var convertedValue = edgeConverter.Convert(value);
            endSocket.SetValue(convertedValue);
        }
    }
}
