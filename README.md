# UnityDots Graph

UnityDots Graph contains an extendible, easy-to-use graph editor for Unity. The package builds on top of Unity's experimental graphview with the goal of making writing new node based editors easier.

![A simple example graph editor created with roughly 50 lines of code.](resources/Graph.png)

## Getting started

To include the package in your Unity project, add the following git package using the Package Manager UI:

```
https://github.example.com/Ieyasu/UnityDots.Graph.git?path=/src"
```

### Creating a Graph

The following snippet defines a new type of graph that can be created using the asset menu. Double clicking the graph asset will then open the graph editor:

```cs
using UnityDots.Graph;
using UnityEngine;

[CreateAssetMenu(fileName = "MyGraph.asset", menuName = "MyGraph", order = 0)]
public sealed class MyGraph : GraphData
{
}
```

### Creating Nodes

The graph by itself is useless. We need to create nodes that do something interesting. First, let's create a socket which is used to connect different nodes together and a common interface for all our nodes:

```cs
using UnityDots.Graph;

// A socket that can input/output an integer
public sealed class MySocket : SocketData<int>
{
}

// A common interface for our nodes
public interface IMyNode : NodeData
{
    void Evaluate();
}
```

Next we'll create a simple node for our custom graph, which takes an integer as an input and increments the integer. If no input is provided, the node simply outputs zero.

```cs
using UnityDots.Graph;
using UnityEngine;

// We need to specify the type of graph the node belongs to, as well as the
// menu path where the node is found in.
[Node(typeof(MyGraph), "IncrementNode")]
public sealed class IncrementNode : NodeData, IMyNode
{
    // An incoming socket that takes an integer
    [Input(0)]
    public MySocket Input { get; set; }

    // An outgoing socket that outputs an integer
    [Output(0)]
    public MySocket Output { get; set; }

    // The node increments the input value by one and outputs the result
    public void Evaluate()
    {
        Output.Value = Input.HasValue ? Input.Value + 1 : 0;
    }
}
```

Finally we create a node that takes an integer as an input and prints it:

```cs
using UnityDots.Graph;
using UnityEngine;

[Node(typeof(MyGraph), "PrintNode")]
public sealed class PrintNode : NodeData, IMyNode
{
    [Input(0)]
    public MySocket Input1 { get; set; }

    [Input(1)]
    public MySocket Input2 { get; set; }

    [Input(2)]
    public MySocket Input3 { get; set; }

    public void Evaluate()
    {
        if (Input1.HasValue)
            Debug.Log("First input: " + Input1.Value);

        if (Input2.HasValue)
            Debug.Log("Second input: " + Input2.Value);

        if (Input3.HasValue)
            Debug.Log("Third input: " + Input3.Value);
    }
}
```

The nodes can now be added to the graph by right clicking anywhere in the graph editor and selecting the desired node from the popup menu that opens up. The nodes can be connected together and moved around.

### Evaluating the Graph

To evaluate the graph, you can either write custom logic or use the prebuilt GraphEvaulator class. The GraphEvaluator will make sure to evaluate the nodes in correct order, starting from nodes with no incoming connections.

```cs
new GraphEvaluator().EvaluateGraph(myGraph, (node) => {
    if (node is IMyNode myNode)
    {
        myNode.Evaluate();
    }
});
```
